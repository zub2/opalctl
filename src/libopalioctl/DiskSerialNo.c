/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DiskSerialNo.h"

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <string.h>

#include <linux/hdreg.h>
#include <linux/nvme_ioctl.h>

// From include/linux/nvme.h
enum nvme_admin_opcode
{
	nvme_admin_identify = 0x06
};

static size_t min(size_t a, size_t b)
{
	return a < b ? a : b;
}

static bool getNvmeSerialNo(int fd, char * serialNo)
{
	uint8_t nvme_id_ctrl[4096];

	struct nvme_admin_cmd cmd =
	{
		.opcode = 0x06, // nvme_admin_identify from include/linux/nvme.h
		.nsid = 0,
		.addr = (unsigned long)&nvme_id_ctrl,
		.data_len = 4096,
		.cdw10 = 1
	};

	if (ioctl(fd, NVME_IOCTL_ADMIN_CMD, &cmd) != -1)
	{
		// First two fields are __le16 vid, __le16 ssvid
		// Followed by char sn[20]
		char * sn = (char *)nvme_id_ctrl + 4;
		const size_t s = min(sizeof(sn), DISK_SERIAL_LENGTH);
		memcpy(serialNo, sn, s);
		if (s < DISK_SERIAL_LENGTH)
			memset(serialNo + s, 0, DISK_SERIAL_LENGTH - s);

		return true;
	}

	return false;
}

static bool getAtaSerialNo(int fd, char * serialNo)
{
	struct hd_driveid id;
	if (ioctl(fd, HDIO_GET_IDENTITY, &id) != -1)
		{
			const size_t s = min(sizeof(id.serial_no), DISK_SERIAL_LENGTH);
			memcpy(serialNo, id.serial_no, s);
			if (s < DISK_SERIAL_LENGTH)
				memset(serialNo + s, 0, DISK_SERIAL_LENGTH - s);

			return true;
		}

	return false;
}

bool getDiskSerialNo(int fd, char * serialNo)
{
	return getAtaSerialNo(fd, serialNo) || getNvmeSerialNo(fd, serialNo);
}
