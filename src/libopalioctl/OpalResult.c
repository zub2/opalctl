/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OpalResult.h"

#include <string.h>
#include <errno.h>

static const char * getOpalErrnoErrorString(int e)
{
	errno = 0;
	const char * s = strerror(e);
	if (errno != 0)
		s = "can't get error description";
	return s;
}

const char * getOpalResultString(const OpalResult * error)
{
	if (error != NULL)
	{
		switch (error->status)
		{
		case OpalSuccess:
			return "success";
		case OpalNotSupported:
			return "IOCTL not supported by kernel (kernel too old or OPAL support not enabled)";
		case OpalNotAnOpalDevice:
			return "not an OPAL device";
		case OpalErrnoError:
			return getOpalErrnoErrorString(error->errNo);
		case OpalInvalidArgument:
			return "libopal function invoked with invalid argument";
		case OpalUnknownError:
		default:
			return "unknown error";
		}
	}
	else
		return "internal error: NULL provided";
}
