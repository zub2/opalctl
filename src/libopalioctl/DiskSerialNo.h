/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DISK_SERIAL_NO_H
#define DISK_SERIAL_NO_H

#include <stdbool.h>

#define DISK_SERIAL_LENGTH 20

/**
 * Retrieve serial number of a block device.
 *
 * This uses HDIO_GET_IDENTITY IOCTL to get the serial number.
 *
 * @param [in] fd File descriptor of the block device to query. It needs read
 * access.
 * @param [out] serialNo Pointer to a buffer where the serial number is to be
 * stored. On success, exactly DISK_SERIAL_LENGTH bytes are written.
 * @return true on success, false on error
 */
bool getDiskSerialNo(int fd, char * serialNo);

#endif // DISK_SERIAL_NO_H
