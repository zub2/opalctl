/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ReadPassword.h"

#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

// https://www.gnu.org/software/libc/manual/html_node/getpass.html
char * readPassword(void)
{
	const int stdInNo = fileno(stdin);

	// turn echoing off and fail if we can’t
	struct termios termiosOld;
	if (tcgetattr (stdInNo, &termiosOld) != 0)
		return NULL;

	struct termios termiosNew = termiosOld;
	termiosNew.c_lflag &= ~ECHO;
	if (tcsetattr(stdInNo, TCSAFLUSH, &termiosNew) != 0)
		return NULL;

	// read the passphrase
	char *passwd = NULL;
	size_t n = 0;
	ssize_t nRead = getline(&passwd, &n, stdin);
	putchar('\n');

	// restore terminal settings
	tcsetattr(stdInNo, TCSAFLUSH, &termiosOld);

	if (nRead < 0 || !passwd)
	{
		free(passwd);
		return NULL;
	}

	// strip trailing newline
	if (nRead > 0 && passwd[nRead-1] == '\n')
		passwd[nRead-1] = 0;

	return passwd;
}
