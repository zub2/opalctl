/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPAL_RESULT_H
#define OPAL_RESULT_H

typedef enum
{
	OpalSuccess,
	OpalNotAnOpalDevice,
	OpalNotSupported,
	OpalErrnoError,
	OpalInvalidArgument,
	OpalUnknownError
} OpalStatus;

typedef struct
{
	OpalStatus status;
	int errNo;
} OpalResult;

/**
 * Retrieve a string describing an OpalResult.
 *
 * Please note that this function is not thread safe because it uses strerror
 * internally.
 *
 * @param [in] error The OpalResult to stringify.
 * @return A static string describing the result. Do not free it.
 */
const char * getOpalResultString(const OpalResult * error);

#endif // OPAL_RESULT_H
