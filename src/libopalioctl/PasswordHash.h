/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of opalctl.
 *
 * opalctl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * opalctl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with opalctl.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PASSWORD_HASH_H
#define PASSWORD_HASH_H

#include <stdint.h>
#include <stddef.h>

#define SEDUTIL_PASSWORD_HASH_SIZE 32

/**
 * Calculate a salted hash of password that is compatible with sedutils.
 *
 * Exactly like sedutils, this uses the cifa library (https://github.com/ctz/cifra)
 * to calulate a PBKDF2-HMAC SHA-1 saltest hash of the password (with 75000
 * PBKFD2 iterations)
 *
 * The implementation of the sedutil part is (at the time of this writing)
 * available here:
 * https://github.com/Drive-Trust-Alliance/sedutil/blob/74d7813e47cac2171680c0209e92f72852329e1a/Common/DtaHashPwd.cpp
 *
 * To produce a hash that matches sedutils:
 *
 * * pass the disk serial number (in a 20bytes long buffer, possibly right-padded
 * with spaces) as the salt; you can use getDiskSerialNo() to ge the serial No.
 *
 * * use SEDUTIL_PASSWORD_HASH_SIZE as the hash size
 *
 * @param [out] hash Where to store the hash.
 * @param [in] hashSize Size of the hash array. Note that sedutil uses the value
 * SEDUTIL_PASSWORD_HASH_SIZE.
 * @param [in] password The password to hash as a null-terminated string.
 * @param [in] salt Array holding the salt. Note that sedutil uses disk serial
 * number.
 * @param [in] saltSize Size of the salt array.
 */
void hashPassword(uint8_t * hash, size_t hashSize, const char * password, const uint8_t * salt, size_t saltSize);

#endif // PASSWORD_HASH_H
